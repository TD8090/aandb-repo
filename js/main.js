/*
 * Copyright 2012 soundarapandian
 * Licensed under the Apache License, Version 2.0
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*

$("document").ready(function() {

*/

/*Start: Prevent the default white background on blur of top navigation *//*

	$(".dropdown-menu li a").mousedown(function() {
		var dropdown = $(this).parents('.dropdown');
		var link = dropdown.children(':first-child');
		link.css('background-color', "#2E3436");
		link.css('color', 'white');
	});
	*/
/*End: Prevent the default white background on blur of top navigation *//*


  */
/*Start : Automatically start the slider *//*

	$('.carousel').carousel({
      interval: 4000
   });
	*/
/*End: Automatically start the slider */


/*
(function($) {
    */
/***
     * Run this code when the #toggle-menu link has been tapped
     * or clicked
     *//*

    $( '.menu-btn' ).on( 'touchstart click', function(e) {
        e.preventDefault();
        var $body = $( 'body' ),
            $page = $( '#navs' ),
            $menu = $( '#menu' ),
        */
/* Cross browser support for CSS "transition end" event *//*

            transitionEnd = 'transitionend webkitTransitionEnd otransitionend MSTransitionEnd';
        */
/* When the toggle menu link is clicked, animation starts *//*

        $body.addClass( 'animating' );
        */
/***
         * Determine the direction of the animation and
         * add the correct direction class depending
         * on whether the menu was already visible.
         *//*

        if ( $body.hasClass( 'menu-visible' ) ) {
            $body.addClass( 'right' );
        } else {
            $body.addClass( 'left' );
        }
        */
/***
         * When the animation (technically a CSS transition)
         * has finished, remove all animating classes and
         * either add or remove the "menu-visible" class
         * depending whether it was visible or not previously.
         *//*

        $page.on( transitionEnd, function() {
            $body
                .removeClass( 'animating left right' )
                .toggleClass( 'menu-visible' );
            $page.off( transitionEnd );
        } );
    } );

*/
/*  if document is clicked during menu show, hide menu  *//*

    $(document).click(function (e) {
        var $container = $("#menu");
        if (!$container.is(e.target) // if the target of the click isn't the container...
            && $container.has(e.target).length === 0) // nor a descendant of the container
        {

            $container.find('.active ul').animate({opacity:'0', easing:'easeOutQuad'}, 500).css({display:'none'});

        }
    });


} )(jQuery);
*/



    /**
     * Author: Heather Corey
     * jQuery Simple Parallax Plugin
     *
     */

//    (function($) {
//
//        $.fn.parallax = function(options) {
//
//            var windowHeight = $(window).height();
//
//            // Establish default settings
//            var settings = $.extend({
//                speed        : 0.15
//            }, options);
//
//            // Iterate over each object in collection
//            return this.each( function() {
//
//                // Save a reference to the element
//                var $this = $(this);
//
//                // Set up Scroll Handler
//                $(document).scroll(function(){
//
//                    var scrollTop = $(window).scrollTop();
//                    var offset = $this.offset().top;
//                    var height = $this.outerHeight();
//
//                    // Check if above or below viewport
//                    if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
//                        return;
//                    }
//
//                    var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
//
//                    // Apply the Y Background Position to Set the Parallax Effect
//                    $this.css('background-position', 'center ' + yBgPosition + 'px');
//
//                });
//            });
//        }
//    }(jQuery));
//
//    $('.bg-1,.bg-3').parallax({
//        speed :	0.15
//    });
//
//    $('.bg-2').parallax({
//        speed :	0.25
//    });









!function ($) {

    "use strict"; // jshint ;_;


    /* DROPDOWN CLASS DEFINITION
     * ========================= */

    var toggle = '[data-toggle=dropdown]'
        , Dropdown = function (element) {
            var $el = $(element).on('click.dropdown.data-api', this.toggle)
            $('html').on('click.dropdown.data-api', function () {
                $el.parent().removeClass('expand')
            })
        }

    Dropdown.prototype = {

        constructor: Dropdown

        , toggle: function (e) {
            var $this = $(this)
                , $parent
                , isActive

            if ($this.is('.disabled, :disabled')) return

            $parent = getParent($this)

            isActive = $parent.hasClass('expand')

            clearMenus()

            if (!isActive) {
                $parent.toggleClass('expand')
                $this.focus()
            }

            return false
        }

        , keydown: function (e) {
            var $this
                , $items
                , $active
                , $parent
                , isActive
                , index

            if (!/(38|40|27)/.test(e.keyCode)) return

            $this = $(this)

            e.preventDefault()
            e.stopPropagation()

            if ($this.is('.disabled, :disabled')) return

            $parent = getParent($this)

            isActive = $parent.hasClass('expand')

            if (!isActive || (isActive && e.keyCode == 27)) return $this.click()

            $items = $('[role=menu] li:not(.divider) a', $parent)

            if (!$items.length) return

            index = $items.index($items.filter(':focus'))

            if (e.keyCode == 38 && index > 0) index--                                        // up
            if (e.keyCode == 40 && index < $items.length - 1) index++                        // down
            if (!~index) index = 0

            $items
                .eq(index)
                .focus()
        }

    }

    function clearMenus() {
        $(toggle).each(function () {
            getParent($(this)).removeClass('expand')
        })
    }

    function getParent($this) {
        var selector = $this.attr('data-target')
            , $parent

        if (!selector) {
            selector = $this.attr('href')
            selector = selector && /#/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
        }

        $parent = $(selector)
        $parent.length || ($parent = $this.parent())

        return $parent
    }


    /* DROPDOWN PLUGIN DEFINITION
     * ========================== */

    $.fn.dropdown = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('dropdown')
            if (!data) $this.data('dropdown', (data = new Dropdown(this)))
            if (typeof option == 'string') data[option].call($this)
        })
    }

    $.fn.dropdown.Constructor = Dropdown


    /* APPLY TO STANDARD DROPDOWN ELEMENTS
     * =================================== */

    $(function () {
        $('html')
            .on('click.dropdown.data-api touchstart.dropdown.data-api', clearMenus)
        $('body')
            .on('click.dropdown touchstart.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
            .on('click.dropdown.data-api touchstart.dropdown.data-api'  , toggle, Dropdown.prototype.toggle)
            .on('keydown.dropdown.data-api touchstart.dropdown.data-api', toggle + ', [role=menu]' , Dropdown.prototype.keydown)
    })

}(window.jQuery);